//
//  ViewController.m
//  GuessGame
//
//  Created by Click Labs134 on 10/20/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

NSArray *countriesNames;
int a=1;
int numberOfAttempts=0;

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIButton *flagImage1;
@property (strong, nonatomic) IBOutlet UIButton *flagImage2;
@property (strong, nonatomic) IBOutlet UIButton *flagImage3;
@property (strong, nonatomic) IBOutlet UIButton *flagImage4;
@property (strong, nonatomic) IBOutlet UIButton *flagImage5;
@property (strong, nonatomic) IBOutlet UIButton *flagImage6;
@property (strong, nonatomic) IBOutlet UIButton *flagImage7;
@property (strong, nonatomic) IBOutlet UIButton *flagImage8;
@property (strong, nonatomic) IBOutlet UIButton *flagImage9;
@property (strong, nonatomic) IBOutlet UIButton *flagImage10;
@property (strong, nonatomic) IBOutlet UIButton *flagImage11;
@property (strong, nonatomic) IBOutlet UIButton *flagImage12;
@property (strong, nonatomic) IBOutlet UIButton *flagImage13;
@property (strong, nonatomic) IBOutlet UIButton *flagImage14;
@property (strong, nonatomic) IBOutlet UIButton *flagImage15;
@property (strong, nonatomic) IBOutlet UIButton *flagImage16;
@property (strong, nonatomic) IBOutlet UIButton *flagImage17;
@property (strong, nonatomic) IBOutlet UIButton *flagImage18;
@property (strong, nonatomic) IBOutlet UIButton *flagImage19;
@property (strong, nonatomic) IBOutlet UIButton *flagImage20;
@property (strong, nonatomic) IBOutlet UIButton *flagImage21;
@property (strong, nonatomic) IBOutlet UIButton *flagImage22;
@property (strong, nonatomic) IBOutlet UIButton *flagImage23;
@property (strong, nonatomic) IBOutlet UIButton *flagImage24;
@property (strong, nonatomic) IBOutlet UIButton *flagImage25;
@property (strong, nonatomic) IBOutlet UIButton *flagImage26;
@property (strong, nonatomic) IBOutlet UIButton *flagImage27;
@property (strong, nonatomic) IBOutlet UIButton *flagImage28;
@property (strong, nonatomic) IBOutlet UIButton *flagImage29;
@property (strong, nonatomic) IBOutlet UIButton *flagImage30;
@property (strong, nonatomic) IBOutlet UIButton *flagImage31;
@property (strong, nonatomic) IBOutlet UIButton *flagImage32;
@property (strong, nonatomic) IBOutlet UIButton *flagImage33;
@property (strong, nonatomic) IBOutlet UIButton *flagImage34;
@property (strong, nonatomic) IBOutlet UIButton *flagImage35;
@property (strong, nonatomic) IBOutlet UIButton *flagImage36;


@property (strong, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) IBOutlet UILabel *countryNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *nextQuestionLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;

@end

@implementation ViewController

@synthesize flagImage1;
@synthesize flagImage2;
@synthesize flagImage3;
@synthesize flagImage4;
@synthesize flagImage5;
@synthesize flagImage6;
@synthesize flagImage7;
@synthesize flagImage8;
@synthesize flagImage9;
@synthesize flagImage10;
@synthesize flagImage11;
@synthesize flagImage12;
@synthesize flagImage13;
@synthesize flagImage14;
@synthesize flagImage15;
@synthesize flagImage16;
@synthesize flagImage17;
@synthesize flagImage18;
@synthesize flagImage19;
@synthesize flagImage20;
@synthesize flagImage21;
@synthesize flagImage22;
@synthesize flagImage23;
@synthesize flagImage24;
@synthesize flagImage25;
@synthesize flagImage26;
@synthesize flagImage27;
@synthesize flagImage28;
@synthesize flagImage29;
@synthesize flagImage30;
@synthesize flagImage31;
@synthesize flagImage32;
@synthesize flagImage33;
@synthesize flagImage34;
@synthesize flagImage35;
@synthesize flagImage36;
@synthesize playButton;
@synthesize countryNameLabel;
@synthesize nextQuestionLabel;
@synthesize questionLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    countriesNames=[[NSArray alloc]init];
    countriesNames=@[@"ASEAN",@"Serbia(Yugoslavia)",@"Bulgaria",@"Lithuania",@"Senegal",@"Croatia",@"Australia",@"Grenada",@"African Union",@"Brazil",@"Luxembourg",@"Netherlands",@"Romania",@"Canada",@"Gambia",@"Vanutau",@"Czech Republic",@"United States of America(USA)",@"Syria",@"Costa Rica",@"Indonezia",@"Bolivia",@"Bermuda",@"Afghanistan",@"Greece",@"Wales",@"Egypt",@"Japan" ,@"Cameroon",@"Macao", @"Honduras",@"Namibia",@"Cape Verde",@"Cook Islands",@"Scotland",@"Botswana"];
    
    //hiding buttons
    
    [flagImage1 setHidden:YES];
    [flagImage2 setHidden:YES];
    [flagImage3 setHidden:YES];
    [flagImage4 setHidden:YES];
    [flagImage5 setHidden:YES];
    [flagImage6 setHidden:YES];
    [flagImage7 setHidden:YES];
    [flagImage8 setHidden:YES];
    [flagImage9 setHidden:YES];
    [flagImage10 setHidden:YES];
    [flagImage11 setHidden:YES];
    [flagImage12 setHidden:YES];
    [flagImage13 setHidden:YES];
    [flagImage14 setHidden:YES];
    [flagImage15 setHidden:YES];
    [flagImage16 setHidden:YES];
    [flagImage16 setHidden:YES];
    [flagImage17 setHidden:YES];
    [flagImage18 setHidden:YES];
    [flagImage19 setHidden:YES];
    [flagImage20 setHidden:YES];
    [flagImage21 setHidden:YES];
    [flagImage22 setHidden:YES];
    [flagImage23 setHidden:YES];
    [flagImage24 setHidden:YES];
    [flagImage25 setHidden:YES];
    [flagImage26 setHidden:YES];
    [flagImage27 setHidden:YES];
    [flagImage28 setHidden:YES];
    [flagImage29 setHidden:YES];
    [flagImage30 setHidden:YES];
    [flagImage31 setHidden:YES];
    [flagImage32 setHidden:YES];
    [flagImage33 setHidden:YES];
    [flagImage34 setHidden:YES];
    [flagImage35 setHidden:YES];
    [flagImage36 setHidden:YES];
    [countryNameLabel setHidden:YES];
    [nextQuestionLabel setHidden:YES];
    [questionLabel setHidden:YES];
    
    nextQuestionLabel.layer.cornerRadius = 20;//half of the width
    nextQuestionLabel.layer.borderColor=[UIColor blueColor].CGColor;
    nextQuestionLabel.layer.borderWidth=2.0f;
    
    //setting uo buttons title
    
    [flagImage1 setTitle:@"Senegal" forState:UIControlStateNormal];
    [flagImage1 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage2 setTitle:@"Australia" forState:UIControlStateNormal];
    [flagImage2 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage3 setTitle:@"Brazil" forState:UIControlStateNormal];
    [flagImage3 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage4 setTitle:@"Canada" forState:UIControlStateNormal];
    [flagImage4 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage5 setTitle:@"Namibia" forState:UIControlStateNormal];
    [flagImage5 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage6 setTitle:@"Romania" forState:UIControlStateNormal];
    [flagImage6 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage7 setTitle:@"Scotland" forState:UIControlStateNormal];
    [flagImage7 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage8 setTitle:@"Syria" forState:UIControlStateNormal];
    [flagImage8 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage9 setTitle:@"Wales" forState:UIControlStateNormal];
    [flagImage9 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage10 setTitle:@"ASEAN" forState:UIControlStateNormal];
    [flagImage10 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage11 setTitle:@"Egypt" forState:UIControlStateNormal];
    [flagImage11 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage12 setTitle:@"Vanutau" forState:UIControlStateNormal];
    [flagImage12 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage13 setTitle:@"Indonezia" forState:UIControlStateNormal];
    [flagImage13 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage14 setTitle:@"Honduras" forState:UIControlStateNormal];
    [flagImage14 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage15 setTitle:@"Grenada" forState:UIControlStateNormal];
    [flagImage15 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage16 setTitle:@"Croatia" forState:UIControlStateNormal];
    [flagImage16 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage17 setTitle:@"Afghanistan" forState:UIControlStateNormal];
    [flagImage17 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage18 setTitle:@"African Union" forState:UIControlStateNormal];
    [flagImage18 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage19 setTitle:@"Bermuda" forState:UIControlStateNormal];
    [flagImage19 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage20 setTitle:@"Bulgaria" forState:UIControlStateNormal];
    [flagImage20 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage21 setTitle:@"Botswana" forState:UIControlStateNormal];
    [flagImage21 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage22 setTitle:@"Cameroon" forState:UIControlStateNormal];
    [flagImage22 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage23 setTitle:@"Cape Verde" forState:UIControlStateNormal];
    [flagImage23 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage24 setTitle:@"Japan" forState:UIControlStateNormal];
    [flagImage24 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage25 setTitle:@"Lithuania" forState:UIControlStateNormal];
    [flagImage25 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage26 setTitle:@"Macao" forState:UIControlStateNormal];
    [flagImage26 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage27 setTitle:@"Serbia(Yugoslavia)" forState:UIControlStateNormal];
    [flagImage27 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage28 setTitle:@"United States of America(USA)" forState:UIControlStateNormal];
    [flagImage28 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage29 setTitle:@"Netherlands" forState:UIControlStateNormal];
    [flagImage29 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage30 setTitle:@"Greece" forState:UIControlStateNormal];
    [flagImage30 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage31 setTitle:@"Luxembourg" forState:UIControlStateNormal];
    [flagImage31 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage32 setTitle:@"Czech Republic" forState:UIControlStateNormal];
    [flagImage32 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage33 setTitle:@"Cook Islands" forState:UIControlStateNormal];
    [flagImage33 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage34 setTitle:@"Costa Rica" forState:UIControlStateNormal];
    [flagImage34 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage35 setTitle:@"Bolivia" forState:UIControlStateNormal];
    [flagImage35 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [flagImage36 setTitle:@"Gambia" forState:UIControlStateNormal];
    [flagImage36 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)playGame:(id)sender
{
    [flagImage1 setHidden:NO];
    [flagImage2 setHidden:NO];
    [flagImage3 setHidden:NO];
    [flagImage4 setHidden:NO];
    [flagImage5 setHidden:NO];
    [flagImage6 setHidden:NO];
    [flagImage7 setHidden:NO];
    [flagImage8 setHidden:NO];
    [flagImage9 setHidden:NO];
    [flagImage10 setHidden:NO];
    [flagImage11 setHidden:NO];
    [flagImage12 setHidden:NO];
    [flagImage13 setHidden:NO];
    [flagImage14 setHidden:NO];
    [flagImage15 setHidden:NO];
    [flagImage16 setHidden:NO];
    [flagImage17 setHidden:NO];
    [flagImage18 setHidden:NO];
    [flagImage19 setHidden:NO];
    [flagImage20 setHidden:NO];
    [flagImage21 setHidden:NO];
    [flagImage22 setHidden:NO];
    [flagImage23 setHidden:NO];
    [flagImage24 setHidden:NO];
    [flagImage25 setHidden:NO];
    [flagImage26 setHidden:NO];
    [flagImage27 setHidden:NO];
    [flagImage28 setHidden:NO];
    [flagImage29 setHidden:NO];
    [flagImage30 setHidden:NO];
    [flagImage31 setHidden:NO];
    [flagImage32 setHidden:NO];
    [flagImage33 setHidden:NO];
    [flagImage34 setHidden:NO];
    [flagImage35 setHidden:NO];
    [flagImage36 setHidden:NO];
    [countryNameLabel setHidden:NO];
    [nextQuestionLabel setHidden:NO];
    [questionLabel setHidden:NO];
    countryNameLabel.alpha = 0.0;
    
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:UIViewAnimationCurveEaseInOut animations:^{
                            
                            countryNameLabel.alpha=10.0;
                        }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:350.0 delay:1.0 options: UIViewAnimationCurveEaseOut animations:^{
                             countryNameLabel.alpha = 0.0;
                     
                         }
                                       completion:^(BOOL finished){
                                              
                                              NSLog(@"Done!");
                                              
                                          }];
                         
                     }];
    questionLabel.alpha = 0.0;
    
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:UIViewAnimationCurveEaseInOut animations:^{
                            
                            questionLabel.alpha=10.0;
                        }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:350.0 delay:1.0 options: UIViewAnimationCurveEaseOut animations:^{
                             questionLabel.alpha = 0.0;
                             
                         }
                                          completion:^(BOOL finished){
                                              
                                              NSLog(@"Done!");
                                              
                                          }];
                         
                     }];
    

}

- (IBAction)clickOnSenegal:(id)sender
{
    if(numberOfAttempts<2)
{
    NSString *buttonName1 = [sender titleForState:UIControlStateNormal];
    if ([buttonName1 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }
    numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnAustralia:(id)sender
{
    if(numberOfAttempts<2)
{
    NSString *buttonName2 = [sender titleForState:UIControlStateNormal];
    if ([buttonName2 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }
        numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnBrazil:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName3 = [sender titleForState:UIControlStateNormal];
    if ([buttonName3 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }
        numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
    
}
- (IBAction)clickOnCanada:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName4 = [sender titleForState:UIControlStateNormal];
    if ([buttonName4 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnNamibia:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName5 = [sender titleForState:UIControlStateNormal];
    if ([buttonName5 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnRomania:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName6 = [sender titleForState:UIControlStateNormal];
    if ([buttonName6 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnScotland:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName7 = [sender titleForState:UIControlStateNormal];
    if ([buttonName7 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnSyria:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName8 = [sender titleForState:UIControlStateNormal];
    if ([buttonName8 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }

}
- (IBAction)clickOnWales:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName9 = [sender titleForState:UIControlStateNormal];
    if ([buttonName9 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnASEAN:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName10 = [sender titleForState:UIControlStateNormal];
    if ([buttonName10 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnEgypt:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName11 = [sender titleForState:UIControlStateNormal];
    if ([buttonName11 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnVantau:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName12 = [sender titleForState:UIControlStateNormal];
    if ([buttonName12 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnIndonezia:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName13 = [sender titleForState:UIControlStateNormal];
    if ([buttonName13 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnHonduras:(id)sender
{
    if (numberOfAttempts<2)
    {
    NSString *buttonName14 = [sender titleForState:UIControlStateNormal];
    if ([buttonName14 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
else
{
    [self alert3];
}
}

- (IBAction)clickOnGrenada:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName15 = [sender titleForState:UIControlStateNormal];
    if ([buttonName15 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnCroatia:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName16 = [sender titleForState:UIControlStateNormal];
    if ([buttonName16 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
    {
        [self alert2];
    }numberOfAttempts++;
}
    else
    {
        [self alert3];
    }
}
- (IBAction)nextQuestion:(id)sender
{
    if (a<=countriesNames.count)
    {
         countryNameLabel.text=[countriesNames objectAtIndex:a];
        [UIView animateWithDuration:1.0
                              delay:0.0
                            options:UIViewAnimationCurveEaseInOut animations:^{
                                
                                countryNameLabel.alpha=10.0;
                            }
                         completion:^(BOOL finished){
                             
                             [UIView animateWithDuration:350.0 delay:1.0 options: UIViewAnimationCurveEaseOut animations:^{
                                 countryNameLabel.alpha = 0.0;
                                 
                             }
                                              completion:^(BOOL finished){
                                                  
                                                  NSLog(@"Done!");
                                                  
                                              }];
                             
                         }];
        a++;
    }
}

- (IBAction)clickOnAfghanistan:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName17 = [sender titleForState:UIControlStateNormal];
        if ([buttonName17 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}

- (IBAction)clickOnAfrican:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName18 = [sender titleForState:UIControlStateNormal];
        if ([buttonName18 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnBermuda:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName19 = [sender titleForState:UIControlStateNormal];
        if ([buttonName19 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnBulgaria:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName20 = [sender titleForState:UIControlStateNormal];
        if ([buttonName20 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnBotswana:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName21 = [sender titleForState:UIControlStateNormal];
        if ([buttonName21 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnCameroon:(id)sender
{
    if (numberOfAttempts<2)
{
    NSString *buttonName22 = [sender titleForState:UIControlStateNormal];
    if ([buttonName22 isEqualToString:countryNameLabel.text])
    {
        [self alert1];
    }
    else
        {
        [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnCape:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName23 = [sender titleForState:UIControlStateNormal];
        if ([buttonName23 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnJapan:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName23 = [sender titleForState:UIControlStateNormal];
        if ([buttonName23 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnLithuania:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName24 = [sender titleForState:UIControlStateNormal];
        if ([buttonName24 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}

- (IBAction)clickOnMacao:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName25 = [sender titleForState:UIControlStateNormal];
        if ([buttonName25 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnSerbia:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName26 = [sender titleForState:UIControlStateNormal];
        if ([buttonName26 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnUSA:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName27 = [sender titleForState:UIControlStateNormal];
        if ([buttonName27 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnNetherlands:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName28 = [sender titleForState:UIControlStateNormal];
        if ([buttonName28 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnGreece:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName29 = [sender titleForState:UIControlStateNormal];
        if ([buttonName29 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnLuxembourg:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName30 = [sender titleForState:UIControlStateNormal];
        if ([buttonName30 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnCzech:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName31 = [sender titleForState:UIControlStateNormal];
        if ([buttonName31 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnCook:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName32 = [sender titleForState:UIControlStateNormal];
        if ([buttonName32 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnCosta:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName33 = [sender titleForState:UIControlStateNormal];
        if ([buttonName33 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnBolivia:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName34 = [sender titleForState:UIControlStateNormal];
        if ([buttonName34 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }
}
- (IBAction)clickOnGambia:(id)sender
{
    if (numberOfAttempts<2)
    {
        NSString *buttonName35 = [sender titleForState:UIControlStateNormal];
        if ([buttonName35 isEqualToString:countryNameLabel.text])
        {
            [self alert1];
        }
        else
        {
            [self alert2];
        }numberOfAttempts++;
    }
    else
    {
        [self alert3];
    }

}

-(void)alert1
{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Correct" message:@"Congratulation" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

-(void)alert2
{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"InCorrect" message:@"Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}
-(void)alert3
{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Wrong" message:@"You Lose" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
